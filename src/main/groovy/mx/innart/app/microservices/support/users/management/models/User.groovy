package mx.innart.app.microservices.support.users.management.models

class User {
    Long id
    String name

    User(Long id, String name) {
        this.id = id
        this.name = name
    }
}
