package mx.innart.app.microservices.support.users.management.services

import mx.innart.app.microservices.support.users.management.models.User
import org.springframework.stereotype.Service

@Service
class UserManagementService {

    List<User> findAllUsers() {
        return [
            new User(1, 'User 1'),
            new User(2, 'User 2'),
            new User(3, 'User 3')
        ]
    }

}
