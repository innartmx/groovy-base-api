package mx.innart.app.microservices.support.users.management.controllers

import mx.innart.app.microservices.support.users.management.models.User
import mx.innart.app.microservices.support.users.management.services.UserManagementService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('users')
class UserManagementController {

    @Autowired
    UserManagementService userManagementService

    @GetMapping
    ResponseEntity<User[]> getUsers() {
        def users = this.userManagementService.findAllUsers()
        return ResponseEntity.ok(users)
    }
}
