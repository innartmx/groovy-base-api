package mx.innart.app

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestAppController {

    @GetMapping('/test')
    ResponseEntity<String> testApp() {
        return ResponseEntity.ok('App is working')
    }
}

