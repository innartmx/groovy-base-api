package mx.innart.app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class BaseApiApplication {

	static void main(String[] args) {
		SpringApplication.run(BaseApiApplication, args)
	}

}

